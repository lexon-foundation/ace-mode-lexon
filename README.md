### Lexon Ace Mode (Syntax Highlighting for Ace/Brace)

[Ace](https://ace.c9.io/) Edit Mode for [Lexon](https://github.com/lexon-foundation/lexon-rust).
